from absl import app
from absl import flags
import numpy as np
import cv2
import cv2.aruco as aruco


flags.DEFINE_integer(
    "aruco_code",
    2,
    "Aruco marker's code",
    short_name="c")
flags.DEFINE_integer(
    "aruco_size",
    700,
    "Aruco marker's size",
    short_name="s")
flags.DEFINE_integer(
    "border_size",
    100,
    "Aruco marker's border size",
    short_name="b")

FLAGS = flags.FLAGS


def create_aruco_marker(aruco_code, aruco_size, border_size):
    aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
    print(aruco_dict)
    # second parameter is id number
    # last parameter is total image size

    img = aruco.drawMarker(aruco_dict, aruco_code, aruco_size)

    # Add a white border around the Aruco code image
    img = cv2.copyMakeBorder(
        img, border_size, border_size, border_size, border_size,
        cv2.BORDER_CONSTANT, None, [255, 255, 255])

    cv2.imwrite(
        "aruco_marker_code={}_size={}_border={}.jpg".format(
            aruco_code,
            aruco_size,
            border_size),
        img)

    # Show the created marker
    # cv2.imshow('frame', img)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()


def start(_):
    aruco_code = FLAGS.aruco_code
    aruco_size = FLAGS.aruco_size
    border_size = FLAGS.border_size
    create_aruco_marker(aruco_code, aruco_size, border_size)


if __name__ == "__main__":
    app.run(start)
