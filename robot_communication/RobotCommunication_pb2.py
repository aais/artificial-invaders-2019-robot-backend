# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: robot_communication/RobotCommunication.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='robot_communication/RobotCommunication.proto',
  package='robotcommunication',
  syntax='proto3',
  serialized_pb=_b('\n,robot_communication/RobotCommunication.proto\x12\x12robotcommunication\">\n\x0bMotorValues\x12\x16\n\x0eleftMotorValue\x18\x01 \x01(\x05\x12\x17\n\x0frightMotorValue\x18\x02 \x01(\x05\"\x18\n\x08\x44oneInfo\x12\x0c\n\x04\x64one\x18\x01 \x01(\x08\x32\x66\n\x11RobotCommunicator\x12Q\n\x0eSetMotorValues\x12\x1f.robotcommunication.MotorValues\x1a\x1c.robotcommunication.DoneInfo\"\x00\x62\x06proto3')
)




_MOTORVALUES = _descriptor.Descriptor(
  name='MotorValues',
  full_name='robotcommunication.MotorValues',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='leftMotorValue', full_name='robotcommunication.MotorValues.leftMotorValue', index=0,
      number=1, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='rightMotorValue', full_name='robotcommunication.MotorValues.rightMotorValue', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=68,
  serialized_end=130,
)


_DONEINFO = _descriptor.Descriptor(
  name='DoneInfo',
  full_name='robotcommunication.DoneInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='done', full_name='robotcommunication.DoneInfo.done', index=0,
      number=1, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=132,
  serialized_end=156,
)

DESCRIPTOR.message_types_by_name['MotorValues'] = _MOTORVALUES
DESCRIPTOR.message_types_by_name['DoneInfo'] = _DONEINFO
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

MotorValues = _reflection.GeneratedProtocolMessageType('MotorValues', (_message.Message,), dict(
  DESCRIPTOR = _MOTORVALUES,
  __module__ = 'robot_communication.RobotCommunication_pb2'
  # @@protoc_insertion_point(class_scope:robotcommunication.MotorValues)
  ))
_sym_db.RegisterMessage(MotorValues)

DoneInfo = _reflection.GeneratedProtocolMessageType('DoneInfo', (_message.Message,), dict(
  DESCRIPTOR = _DONEINFO,
  __module__ = 'robot_communication.RobotCommunication_pb2'
  # @@protoc_insertion_point(class_scope:robotcommunication.DoneInfo)
  ))
_sym_db.RegisterMessage(DoneInfo)



_ROBOTCOMMUNICATOR = _descriptor.ServiceDescriptor(
  name='RobotCommunicator',
  full_name='robotcommunication.RobotCommunicator',
  file=DESCRIPTOR,
  index=0,
  options=None,
  serialized_start=158,
  serialized_end=260,
  methods=[
  _descriptor.MethodDescriptor(
    name='SetMotorValues',
    full_name='robotcommunication.RobotCommunicator.SetMotorValues',
    index=0,
    containing_service=None,
    input_type=_MOTORVALUES,
    output_type=_DONEINFO,
    options=None,
  ),
])
_sym_db.RegisterServiceDescriptor(_ROBOTCOMMUNICATOR)

DESCRIPTOR.services_by_name['RobotCommunicator'] = _ROBOTCOMMUNICATOR

# @@protoc_insertion_point(module_scope)
