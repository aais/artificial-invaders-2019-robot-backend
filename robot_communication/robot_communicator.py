import grpc

import robot_communication.RobotCommunication_pb2 as rc_pb2
import robot_communication.RobotCommunication_pb2_grpc as rc_pb2_grpc


class RobotCommunicator:
    def __init__(self, host_ip, port):
        self._host_ip = host_ip
        self._server_port = port
        self._channel = grpc.insecure_channel(
            '{}:{}'.format(self._host_ip, self._server_port))
        self._stub = rc_pb2_grpc.RobotCommunicatorStub(self._channel)

    def send_motor_values(
            self,
            left_motor_value,
            right_motor_value):
        '''
        Send motor values to robot
        '''
        motor_values = rc_pb2.MotorValues(
            leftMotorValue=left_motor_value,
            rightMotorValue=right_motor_value)
        response = self._stub.SetMotorValues(motor_values)
        return response.done
