import grpc

import unity_communication.UnityCommunication_pb2 as uc_pb2
import unity_communication.UnityCommunication_pb2_grpc as uc_pb2_grpc


class UnityCommunicator:
    def __init__(self, host_ip, port):
        self._host_ip = host_ip
        self._server_port = port
        self._channel = grpc.insecure_channel(
            '{}:{}'.format(self._host_ip, self._server_port))
        self._stub = uc_pb2_grpc.UnityCommunicatorStub(self._channel)

    def send_observations(
            self,
            striker_dynamic_obs,
            striker_static_obs,
            goalie_dynamic_obs,
            goalie_static_obs):
        '''
        Send observations to Unity brain via gRPC
        '''
        observations = uc_pb2.Observations(
            strikerDynamicObservations=striker_dynamic_obs,
            strikerStaticObservations=striker_static_obs,
            goalieDynamicObservations=goalie_dynamic_obs,
            goalieStaticObservations=goalie_static_obs)
        response = self._stub.SendObservations(observations)
        return response.done

    def get_actions(self):
        '''
        Get latest action from Unity brain via gRPC
        '''
        response = self._stub.GetActions(uc_pb2.NoParams())
        return response.strikerAction, response.goalieAction
