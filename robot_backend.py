from absl import app
from absl import logging
from absl import flags
import time
import sys
import traceback
from multiprocessing import Process, Manager

from utils import parse_options
from robot_game.robot_game import (
    RobotGame,
    print_all_observations)
from unity_communication.unity_communicator import UnityCommunicator
from robot_reporter.robot_reporter_servicer import RobotReporterServicer

from robot_reporter.RobotReporter_pb2 import STRIKER, GOALIE


flags.DEFINE_string(
    "mode",
    "prod",
    "Specify operation mode. Either: 'test' or 'prod'",
    short_name="m")

flags.DEFINE_string(
    "params_file",
    "params.yaml",
    "Specify the path to params.yaml file",
    short_name="p")


class RobotBackend():
    def __init__(self, params, robot_infos):
        self._game = RobotGame(
            params["game_params"],
            params["ip_camera_params"],
            robot_infos)

        self._unity_com = UnityCommunicator(
            params["unity_params"]["unity_ip"],
            params["unity_params"]["unity_port"])

    def make_action(self, action, agent_type):
        command_send = self._game.do_action(action, agent_type)
        return command_send

    def get_observations(
            self,
            striker_aruco_code,
            goalie_aruco_code,
            debug=False):
        """
        Returns:
            striker_dynamic_obs,
            striker_static_obs,
            goalie_dynamic_obs,
            goalie_static_obs
        """
        return self._game.get_observations(
            striker_aruco_code,
            goalie_aruco_code,
            debug)

    def get_actions(
            self,
            striker_dynamic_obs,
            striker_static_obs,
            goalie_dynamic_obs,
            goalie_static_obs):
        """
        Send observations to Unity and after a wait ask for actions
        for both striker and goalie
        """
        try:
            result = self._unity_com.send_observations(
                striker_dynamic_obs,
                striker_static_obs,
                goalie_dynamic_obs,
                goalie_static_obs)
            if result is False:
                raise Exception("Could not send observations to Unity Brain")
        except Exception:
            logging.error("Could not connect to Unity via gRPC")
            pass
        """
        Wait for the Unity Brain to make a
        prediction after sending observations
        """
        striker_action = -1
        goalie_action = -1
        time.sleep(0.02)
        counter = 0
        while counter < 10:
            temp_striker_action, temp_goalie_action = \
                self._unity_com.get_actions()
            if temp_striker_action > -1 and temp_goalie_action > -1:
                striker_action = temp_striker_action
                goalie_action = temp_goalie_action
                break

            time.sleep(0.01)
            counter += 1
        if counter > 9:
            raise Exception("Cannot get proper action from Unity")
        return striker_action, goalie_action

    def close(self):
        self._game.close()


def wait_for_robot_reporting(port):
    robot_infos = Manager().dict()
    server = RobotReporterServicer(
        port,
        robot_infos)
    server.start_server()
    count = 1
    while len(robot_infos.keys()) < 1:
        print(
            "=== Waiting for robots to report to backend: {}".format(count),
            end="\r")
        time.sleep(1)
        count += 1
    print("=== Striker and Goalie reported to backend")
    server.close_server()
    return robot_infos


def main(_):
    params_file = FLAGS.params_file
    params = parse_options(params_file)

    mode = FLAGS.mode

    print_end_char = "\n"
    robot_infos = None
    if mode == "prod":
        print_end_char = "\r"
        robot_infos = wait_for_robot_reporting(
            params["robot_reporter"]["port"])
    try:
        robot_backend = RobotBackend(params, robot_infos)
    except Exception as error:
        print(error)
        quit()

    striker_aruco_code = \
        params["game_params"]["agent_params"]["striker"]["aruco_code"]
    goalie_aruco_code = \
        params["game_params"]["agent_params"]["goalie"]["aruco_code"]

    try:
        while True:
            start = time.time()

            (striker_dynamic_obs,
             striker_static_obs,
             goalie_dynamic_obs,
             goalie_static_obs) = \
                robot_backend.get_observations(
                    striker_aruco_code,
                    goalie_aruco_code,
                    debug=True)
            observation_time = time.time()

            striker_action, goalie_action = robot_backend.get_actions(
                striker_dynamic_obs,
                striker_static_obs,
                goalie_dynamic_obs,
                goalie_static_obs)
            action_getting_time = time.time()

            sending_action = None
            if mode == "prod":
                striker_command_send = \
                    robot_backend.make_action(striker_action, STRIKER)
                goalie_command_send = \
                    robot_backend.make_action(goalie_action, GOALIE)
                sending_action = time.time()
            elif mode == "test":
                print_all_observations(
                    striker_dynamic_obs,
                    striker_static_obs,
                    goalie_dynamic_obs,
                    goalie_static_obs,
                    include_raw=True)
                time.sleep(0.02)
            else:
                raise ValueError(
                    "Unknown mode '{}'. Should be 'test' or 'prod'".format(
                        mode))

            end = time.time()
            print(
                """FPS: {:02}, TotalTime: {:01.3f}, ObsTime: {:01.3f}ms, UnityTime: {:01.3f}ms, SendingTime: {:01.3f}ms""".format(
                    int(1 / (end - start)),
                    end - start,
                    observation_time - start,
                    action_getting_time - observation_time,
                    sending_action - action_getting_time if sending_action else 0),
                end=print_end_char)

    except KeyboardInterrupt:
        print("Keyboard command to exit")

    except Exception:
        etype, evalue, tb = sys.exc_info()
        stacktrace = ''.join(traceback.format_exception(etype, evalue, tb))
        message = 'Error in environment process: {}'.format(stacktrace)
        logging.error(message)

    finally:
        if mode == "prod":
            # Stop robot's motors
            robot_backend.make_action(0, STRIKER)
            robot_backend.make_action(0, GOALIE)
        print("Exiting")
        robot_backend.close()
        quit()


if __name__ == "__main__":
    FLAGS = flags.FLAGS
    app.run(main)
