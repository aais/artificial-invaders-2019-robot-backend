import math
import json
import numpy as np


from robot_game.observation_utils import (
    get_sector_observation_for_dynamic_objects,
    get_ray_observation_for_static_objects,
    generate_wall_and_goal_lines)
from ip_cam_connection.ip_cam_connector import IPCamConnection
from robot_communication.robot_communicator import RobotCommunicator
from robot_reporter.RobotReporter_pb2 import STRIKER, GOALIE


NUMBER_OF_ACTIONS = 5


class RobotGame():
    def __init__(self, game_params, ip_camera_params, robot_infos):
        self._angles = game_params["angles"]
        self._ray_length = game_params["ray_length"]
        self._robot_speed = game_params["robot_speed"]
        self._turn_speed = game_params["turn_speed"]
        if ip_camera_params is not None:
            self._ip_cam_connection = IPCamConnection(ip_camera_params)
            self._ip_cam_connection.start()
        else:
            self._ip_cam_connection = None
            print("Camera not setup")

        if robot_infos and STRIKER in robot_infos.keys():
            self._striker_robotCommunicator = RobotCommunicator(
                robot_infos[STRIKER]["ip_address"],
                robot_infos[STRIKER]["port"])
            print("Striker RobotCommunicator initialized")
        if robot_infos and GOALIE in robot_infos.keys():
            self._goalie_robotCommunicator = RobotCommunicator(
                robot_infos[GOALIE]["ip_address"],
                robot_infos[GOALIE]["port"])
            print("Goalie RobotCommunicator initialized")
        self._walls_and_goals_lines = generate_wall_and_goal_lines(
            game_params["arena_params_path"],
            game_params["team_color"])

    def get_observations(
            self,
            striker_aruco_code,
            goalie_aruco_code,
            debug=False):
        """
        Get dynamic and static observations for both striker and goalie robot
        """
        (striker_pos,
         striker_rot,
         goalie_pos,
         goalie_rot,
         enemy_positions,
         good_ball_positions,
         bad_ball_positions) = \
            self._ip_cam_connection.get_dynamic_item_coordinates(
                striker_aruco_code, goalie_aruco_code, debug)

        object_coordinates = [
            good_ball_positions,
            bad_ball_positions,
            enemy_positions]

        striker_dynamic_obs = None
        if striker_pos is not None and striker_pos.any():
            striker_objects = object_coordinates[:]
            striker_objects.append(np.array([goalie_pos], dtype=np.float32))
            striker_dynamic_obs = self._get_agent_dynamic_observations(
                striker_pos, striker_rot, striker_objects)

        goalie_dynamic_obs = None
        if goalie_pos is not None and goalie_pos.any():
            goalie_objects = object_coordinates[:]
            goalie_objects.append(np.array([striker_pos], dtype=np.float32))
            goalie_dynamic_obs = self._get_agent_dynamic_observations(
                goalie_pos, goalie_rot, goalie_objects)

        striker_static_obs = None
        if striker_pos is not None and striker_pos.any():
            striker_static_obs = self._get_agent_static_observations(
                striker_pos,
                striker_rot,
                self._ray_length,
                self._walls_and_goals_lines)

        goalie_static_obs = None
        if goalie_pos is not None and goalie_pos.any():
            goalie_static_obs = self._get_agent_static_observations(
                goalie_pos,
                goalie_rot,
                self._ray_length,
                self._walls_and_goals_lines)

        return (
            striker_dynamic_obs,
            striker_static_obs,
            goalie_dynamic_obs,
            goalie_static_obs)

    def _get_local_position_and_angles(self, robot_rotation, angles):
        local_rotation = robot_rotation - 90
        local_angles = angles
        for i in range(len(local_angles)):
            local_angles[i] = local_angles[i] - local_rotation + 90

        return local_angles

    def _get_agent_dynamic_observations(
            self,
            agent_pos,
            agent_rot,
            object_coordinates):
        local_angles = self._get_local_position_and_angles(
                agent_rot,
                self._angles[:])

        return get_sector_observation_for_dynamic_objects(
            agent_pos,
            local_angles,
            self._ray_length,
            object_coordinates)

    def get_dynamic_item_observation(
            self,
            striker_aruco_code,
            goalie_aruco_code,
            debug=False):
        """
        For debuging. get_observations-function
        combines ball and other observations
        """
        (striker_pos,
         striker_rot,
         goalie_pos,
         goalie_rot,
         enemy_positions,
         good_ball_positions,
         bad_ball_positions) = \
            self._ip_cam_connection.get_dynamic_item_coordinates(
                striker_aruco_code, goalie_aruco_code, debug)

        object_coordinates = [
            good_ball_positions,
            bad_ball_positions,
            enemy_positions]

        striker_obs = None
        if striker_pos is not None and striker_pos.any():
            striker_objects = object_coordinates[:]
            striker_objects.append(np.array([goalie_pos], dtype=np.float32))
            striker_obs = self._get_agent_dynamic_observations(
                striker_pos, striker_rot, striker_objects)

        goalie_obs = None
        if goalie_pos is not None and goalie_pos.any():
            goalie_objects = object_coordinates[:]
            goalie_objects.append(np.array([striker_pos], dtype=np.float32))
            goalie_obs = self._get_agent_dynamic_observations(
                goalie_pos, goalie_rot, goalie_objects)

        return striker_obs, goalie_obs

    def _get_agent_static_observations(
            self,
            agent_pos,
            agent_rot,
            ray_length,
            walls_and_goals_lines):
        local_angles = self._get_local_position_and_angles(
                agent_rot,
                self._angles[:])

        return get_ray_observation_for_static_objects(
            agent_pos,
            local_angles,
            ray_length,
            walls_and_goals_lines)

    def get_static_item_observation(
            self,
            striker_aruco_code,
            goalie_aruco_code,
            debug=False):
        """
        For debuging. get_observations-function
        combines ball and other observations
        """
        (striker_pos,
         striker_rot,
         goalie_pos,
         goalie_rot,
         _,
         __,
         ___) = \
            self._ip_cam_connection.get_dynamic_item_coordinates(
                striker_aruco_code, goalie_aruco_code, debug)

        striker_obs = None
        if striker_pos is not None and striker_pos.any():
            striker_obs = self._get_agent_static_observations(
                striker_pos,
                striker_rot,
                self._ray_length,
                self._walls_and_goals_lines)

        goalie_obs = None
        if goalie_pos is not None and goalie_pos.any():
            goalie_obs = self._get_agent_static_observations(
                goalie_pos,
                goalie_rot,
                self._ray_length,
                self._walls_and_goals_lines)

        return striker_obs, goalie_obs

    def do_action(self, action, agent_type):
        command_send = False
        left_motor, right_motor = self._get_motor_speeds(action)
        if agent_type is STRIKER:
            command_send = self._set_robot_motors(
                left_motor,
                right_motor,
                self._striker_robotCommunicator)
        elif agent_type is GOALIE:
            command_send = self._set_robot_motors(
                left_motor,
                right_motor,
                self._goalie_robotCommunicator)
        else:
            raise ValueError("Unknown agent_type: {}".format(agent_type))

        return command_send

    def number_of_observations(self):
        return len(self.get_observations())

    def get_number_of_actions(self):
        return NUMBER_OF_ACTIONS

    def _set_robot_motors(self, l_motor, r_motor, robot_communicator):
        command_send = False
        if l_motor is not None and r_motor is not None:
            command_send = \
                robot_communicator.send_motor_values(l_motor, r_motor)
        return command_send

    def _get_motor_speeds(self, action):
        l_motor = None
        r_motor = None

        # Forward
        if action == 1:
            # negative speed is forward in bot's orientation
            l_motor = -self._robot_speed
            r_motor = -self._robot_speed
        # Backward
        elif action == 2:
            l_motor = self._robot_speed
            r_motor = self._robot_speed
        # Turn Clockwise
        elif action == 3:
            l_motor = -self._robot_speed * self._turn_speed
            r_motor = self._robot_speed * self._turn_speed
        # Turn Anti Clockwise
        elif action == 4:
            l_motor = self._robot_speed * self._turn_speed
            r_motor = -self._robot_speed * self._turn_speed
        # Forward plus turn right
        elif action == 5:
            l.motor = self._robot_speed
            r.motor = self._robot_speed* 0.5
        #Forward plus turn left
        elif action == 6:
            l.motor = self._robot_speed* 0.5
            r.motor = self._robot_speed
        # No action
        elif action == 0:
            l_motor = 0
            r_motor = 0
        else:
            raise Exception("Unknown action {}".format(action))
        # print("Action {}, L: {}, R: {}".format(
        #     action, int(l_motor), int(r_motor)))
        return int(l_motor), int(r_motor)

    def close(self):
        if self._ip_cam_connection is not None:
            self._ip_cam_connection.close()


# For testing
def print_dynamic_item_observations(
        obs,
        return_string=False,
        include_raw=False):
    if obs:
        obs[::6] = ['%.0f' % elem for elem in obs[::6]]
        obs[1::6] = ['%.0f' % elem for elem in obs[1::6]]
        obs[2::6] = ['%.0f' % elem for elem in obs[2::6]]
        obs[3::6] = ['%.0f' % elem for elem in obs[3::6]]
        obs[4::6] = ['%.0f' % elem for elem in obs[4::6]]
        obs[5::6] = ['%.2f' % elem for elem in obs[5::6]]
    obs_string = "==== Sector 0: {} | 90-45 right\n".format(
        obs[0:6] if obs else "")
    obs_string += "==== Sector 1: {} | 45-20 right\n".format(
        obs[6:12] if obs else "")
    obs_string += "==== Sector 2: {} | 20-0 right\n".format(
        obs[12:18] if obs else "")
    obs_string += "==== Sector 3: {} | 20-0 left\n".format(
        obs[18:24] if obs else "")
    obs_string += "==== Sector 4: {} | 45-20 left\n".format(
        obs[24:30] if obs else "")
    obs_string += "==== Sector 5: {} | 90-45 left\n".format(
        obs[30:36] if obs else "")

    if include_raw:
        obs_string += "==== Raw observation\n{}\n".format(obs)

    if return_string:
        return obs_string
    else:
        print(obs_string)
        return ""


# For testing
def print_static_item_observations(
        obs,
        return_string=False,
        include_raw=True):
    if obs:
        obs[::5] = ['%.0f' % elem for elem in obs[::5]]
        obs[1::5] = ['%.0f' % elem for elem in obs[1::5]]
        obs[2::5] = ['%.0f' % elem for elem in obs[2::5]]
        obs[3::5] = ['%.0f' % elem for elem in obs[3::5]]
        obs[4::5] = ['%.2f' % elem for elem in obs[4::5]]
    obs_string = "==== Sector 0: {} | 90 right\n".format(
        obs[0:5] if obs else "")
    obs_string += "==== Sector 1: {} | 45 right\n".format(
        obs[5:10] if obs else "")
    obs_string += "==== Sector 2: {} | 20 right\n".format(
        obs[10:15] if obs else "")
    obs_string += "==== Sector 3: {} | forward\n".format(
        obs[15:20] if obs else "")
    obs_string += "==== Sector 4: {} | 20 left\n".format(
        obs[20:25] if obs else "")
    obs_string += "==== Sector 5: {} | 45 left\n".format(
        obs[25:30] if obs else "")
    obs_string += "==== Sector 6: {} | 90 left\n".format(
        obs[30:35] if obs else "")

    if include_raw:
        obs_string += "==== Raw observation\n{}\n".format(obs)

    if return_string:
        return obs_string
    else:
        print(obs_string)
        return ""


def print_all_observations(
        striker_dynamic_obs,
        striker_static_obs,
        goalie_dynamic_obs,
        goalie_static_obs,
        return_string=False,
        include_raw=False):
    obs_string = "==== STRIKER OBSERVATIONS\n"
    obs_string += "==== Dynamic observations\n"
    obs_string += print_dynamic_item_observations(
        striker_dynamic_obs,
        return_string=True,
        include_raw=True)
    obs_string += "==== Static observations\n"
    obs_string += print_static_item_observations(
        striker_static_obs,
        return_string=True,
        include_raw=True)
    obs_string += "==== GOALIE OBSERVATIONS\n"
    obs_string += "==== Dynamic observations\n"
    obs_string += print_dynamic_item_observations(
        goalie_dynamic_obs,
        return_string=True,
        include_raw=True)
    obs_string += "==== Static observations\n"
    obs_string += print_static_item_observations(
        goalie_static_obs,
        return_string=True,
        include_raw=True)
    obs_string += "===\n"

    if return_string:
        return obs_string
    else:
        print(obs_string)
        return ""


# For testing
# Run this with "python -m robot_controller.robot_controller"
# From the project's root folder
def main(_):
    """
    This test code has been made to work with game_params's
    angles-variable to have 7 angles with the specific angle values
    which are shown in the print commands.
    """
    mode = FLAGS.mode
    params_file = FLAGS.params_file
    params = parse_options(params_file)
    try:
        robot_game = RobotGame(
            params["game_params"],
            params["ip_camera_params"],
            None)
    except Exception as error:
        print(error)
        quit()

    striker_aruco_code = \
        params["game_params"]["agent_params"]["striker"]["aruco_code"]
    goalie_aruco_code = \
        params["game_params"]["agent_params"]["goalie"]["aruco_code"]

    try:
        if mode == "dynamic":
            while True:
                striker_obs, goalie_obs = \
                    robot_game.get_dynamic_item_observation(
                        striker_aruco_code,
                        goalie_aruco_code,
                        debug=True)
                print("=== STRIKER OBSERVATIONS")
                print_dynamic_item_observations(striker_obs)
                print("=== GOALIE OBSERVATIONS")
                print_dynamic_item_observations(goalie_obs)
                print("===")
        elif mode == "static":
            while True:
                striker_obs, goalie_obs = \
                    robot_game.get_static_item_observation(
                        striker_aruco_code,
                        goalie_aruco_code,
                        debug=True)
                print("=== STRIKER OBSERVATIONS")
                print_static_item_observations(striker_obs)
                print("=== GOALIE OBSERVATIONS")
                print_static_item_observations(goalie_obs)
                print("===")
        elif mode == "both":
            while True:
                (striker_dynamic_obs,
                 striker_static_obs,
                 goalie_dynamic_obs,
                 goalie_static_obs) = \
                    robot_game.get_observations(
                        striker_aruco_code,
                        goalie_aruco_code,
                        debug=True)
                obs_string = print_all_observations(
                    striker_dynamic_obs,
                    striker_static_obs,
                    goalie_dynamic_obs,
                    goalie_static_obs,
                    return_string=True)
                # os.system('clear')
                # sys.stdout.write('{0} imported\r'.format(obs_string))
                # sys.stdout.flush()
                print(obs_string)

        else:
            raise ValueError("Unknown test print mode")

    except KeyboardInterrupt:
        print("Closing")

    except Exception as error:
        etype, evalue, tb = sys.exc_info()
        stacktrace = ''.join(traceback.format_exception(etype, evalue, tb))
        message = 'Error in environment process: {}'.format(stacktrace)
        logging.error(message)

    finally:
        robot_game.close()
        quit()


if __name__ == "__main__":
    from absl import app
    from absl import logging
    from absl import flags
    from utils import parse_options
    import sys
    import os
    import traceback

    flags.DEFINE_string(
        "mode",
        "dynamic",
        "Specify test pint mode. Either: 'dynamic', 'static' or 'both",
        short_name="m")
    flags.DEFINE_string(
        "params_file",
        "params.yaml",
        "Specify the path to params.yaml file",
        short_name="p")

    FLAGS = flags.FLAGS
    app.run(main)
