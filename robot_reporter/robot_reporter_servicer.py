import grpc
from concurrent import futures
import time
import random
import atexit
import json
from multiprocessing import Process

import robot_reporter.RobotReporter_pb2 as rr_pb2
import robot_reporter.RobotReporter_pb2_grpc as rr_pb2_grpc

from robot_reporter.RobotReporter_pb2 import STRIKER, GOALIE


STRIKER_NAME = "Striker"
GOALIE_NAME = "Goalie"


class RobotReporterServicer(rr_pb2_grpc.RobotReporterServicer):
    def __init__(self, robot_reporter_port, robot_infos):
        self._robot_reporter_port = robot_reporter_port
        self._robot_infos = robot_infos
        self._process = None
        # atexit.register(self.close_server)

    def _start_server(self):
        self._robot_reporter_server = grpc.server(
            futures.ThreadPoolExecutor(max_workers=1))
        rr_pb2_grpc.add_RobotReporterServicer_to_server(
            self, self._robot_reporter_server)
        self._robot_reporter_server.add_insecure_port(
            "[::]:{}".format(self._robot_reporter_port))
        self._robot_reporter_server.start()
        print("=== Robot Reporter server running at port {} ...".format(
            self._robot_reporter_port))

        try:
            while True:
                time.sleep(60*60*60)
        except KeyboardInterrupt:
            print("Robot Reporter server Stopped ...")
        finally:
            self._robot_reporter_server.stop(0)

    def start_server(self):
        self._process = Process(target=self._start_server)
        self._process.start()

    def close_server(self):
        print("=== Closing Robot Reporter server ...")
        self._process.terminate()

    def GetRobotInfo(self, request, context):
        robot_ip_address = request.ipAddress
        robot_port = request.port
        agent_type = request.agentType

        if agent_type not in self._robot_infos.keys():
            self._robot_infos[agent_type] = {
                "ip_address": robot_ip_address,
                "port": robot_port}

            print("{} at {}:{}".format(
                STRIKER_NAME if agent_type == STRIKER else GOALIE_NAME,
                robot_ip_address,
                robot_port))

        else:
            print("{} already in robot_infos".format(agent_type))

        return rr_pb2.NoParams()

# Testing Robot Reporter
if __name__ == '__main__':
    from multiprocessing import Manager
    from utils import parse_options

    robot_infos = Manager().dict()

    params = parse_options("params.yaml")
    server = RobotReporterServicer(
        params["robot_reporter"]["port"],
        robot_infos)
    server.start_server()

    try:
        while True:
            time.sleep(60*60*60)
    except KeyboardInterrupt:
        print("Stopping Robot Reporter server ...")
