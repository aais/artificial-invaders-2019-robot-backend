from absl import app
from absl import flags

from utils import parse_options
from robot_communication.robot_communicator import RobotCommunicator


flags.DEFINE_string(
        "params_file",
        "params.yaml",
        "Specify the path to params.yaml file",
        short_name="p")

FLAGS = flags.FLAGS


def main(_):
    params_file = FLAGS.params_file
    params = parse_options(params_file)

    striker_communicator = RobotCommunicator(
      params["game_params"]["agent_params"]["striker"]["ip_address"],
      params["game_params"]["agent_params"]["striker"]["port"]
    )

    goalie_communicator = RobotCommunicator(
      params["game_params"]["agent_params"]["goalie"]["ip_address"],
      params["game_params"]["agent_params"]["goalie"]["port"]
    )

    count = 0
    while count < 10:
        goalie_done = \
            goalie_communicator.send_motor_values(150, 150)
        striker_done = \
            striker_communicator.send_motor_values(150, 150)
        print(
          """Goalie says: {}\nStriker says:{}\n""".format(
                goalie_done, striker_done))
        count += 1


if __name__ == "__main__":
    app.run(main)
