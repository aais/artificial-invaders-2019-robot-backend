from threading import Thread, RLock
from queue import Queue
import cv2
from cv2 import aruco
import numpy as np
import imutils
import json
import math
import time
from ip_cam_connection.image_utils import (
    get_aruco_marker_pos_and_rot)


class IPCamConnection():
    def __init__(self, ip_camera_params):
        self._ip_camera_params = ip_camera_params
        self._video_url = ip_camera_params["video_url"]
        self._aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_250)
        self._parameters = aruco.DetectorParameters_create()
        self._camera_calib_params = None
        with open(ip_camera_params["calibration_params_file"]) as json_file:
            self._camera_calib_params = json.load(json_file)

        self._vectorized_scale_coordinate = np.vectorize(
            self._scale_coordinate)

        self._original_video_height = \
            self._ip_camera_params["original_video_height"]
        self._original_video_width = \
            self._ip_camera_params["original_video_width"]
        self._cropped_video_size = \
            self._ip_camera_params["cropped_video_size"]
        self._arena_size_in_meters = \
            self._ip_camera_params["arena_size_in_meters"]

        self._low_pink_ball_color = np.array(
            self._ip_camera_params["low_pink_ball_color_hsv"],
            dtype=np.float32)
        self._high_pink_ball_color = np.array(
            self._ip_camera_params["high_pink_ball_color_hsv"],
            dtype=np.float32)

        self._low_yellow_ball_color = np.array(
            self._ip_camera_params["low_yellow_ball_color_hsv"],
            dtype=np.float32)
        self._high_yellow_ball_color = np.array(
            self._ip_camera_params["high_yellow_ball_color_hsv"],
            dtype=np.float32)

        self._lock = RLock()
        self._latest_image = None
        self._read_next_image = True

    def _read_image_to_queue(self):
        video_capture = cv2.VideoCapture(self._video_url)
        while True and self._read_next_image is True:
            success, image_frame = video_capture.read()
            if success is False:
                raise Exception(
                    "Cannot connect to video url: {}".format(self._video_url))
            with self._lock:
                self._latest_image = image_frame

        with self._lock:
            self._latest_image = None

        print("Realeasing the video...")
        video_capture.release()

    def start(self):
        self._image_reader = Thread(target=self._read_image_to_queue, args=())
        self._image_reader.start()
        # Wait for the video capturing to initialize
        time.sleep(2)

    def close(self):
        cv2.destroyAllWindows()
        self._read_next_image = False
        self._image_reader.join()

    def _resize_image(self, image):
        return cv2.resize(
            image,
            (
                int(self._ip_camera_params["resize_video_size"]),
                int(self._ip_camera_params["resize_video_size"])
            ))

    def _crop_image(self, image):
        side_band_x = int(
            (self._original_video_width - self._cropped_video_size) / 2)
        start_x = side_band_x
        end_x = self._cropped_video_size + side_band_x

        side_band_y = int(
            (self._original_video_height - self._cropped_video_size) / 2)
        start_y = side_band_y
        end_y = self._cropped_video_size + side_band_y

        # startY and endY coordinates, startX and endX coordinates
        cropped = image[start_y:end_y, start_x:end_x]
        return cropped

    @staticmethod
    def _find_balls_by_color(hsv_image, orig_image, low_color, high_color):
        color_mask = cv2.inRange(hsv_image, low_color, high_color)
        color_mask = cv2.erode(color_mask, None, iterations=2)
        color_mask = cv2.dilate(color_mask, None, iterations=2)
        color_image = cv2.bitwise_and(orig_image, orig_image, mask=color_mask)
        return color_image, color_mask

    @staticmethod
    def _blur_and_hsv(image):
        blurred_frame = cv2.GaussianBlur(image, (5, 5), 0)
        hsv_image = cv2.cvtColor(blurred_frame, cv2.COLOR_BGR2HSV)
        return hsv_image

    def _find_center_points(self, color_mask, orig_image=None):
        center_points = []
        contours = cv2.findContours(
            color_mask, cv2.RETR_EXTERNAL,
            cv2.CHAIN_APPROX_SIMPLE)
        contours = imutils.grab_contours(contours)

        for contour in contours:
            # Uncomment the below print to see the right
            # value for min_ball_area_to_detect
            # print("Ball area detected: {}".format(cv2.contourArea(contour)))
            if (cv2.contourArea(contour) <
                    self._ip_camera_params["min_ball_area_to_detect"]):
                continue

            if orig_image is not None:
                cv2.drawContours(orig_image, contour, -1, (0, 255, 0), 3)
            # compute the center of the contour
            moments = cv2.moments(contour)
            center_x = int(moments["m10"] / moments["m00"])
            center_y = int(moments["m01"] / moments["m00"])
            center_points.append([center_x, center_y])
        return np.array(center_points, dtype=np.float32)

    def _scale_coordinate(self, coordinate):
        """
        Scale one camera pixel coordinate to value between
        "-arena_size_in_meters/2" and "arena_size_in_meters/2"
        """
        # Scale pixel coordinate from -1 to 1
        scaled = \
            (coordinate - self._cropped_video_size / 2) \
            / (self._cropped_video_size / 2)

        # Scale from -1 - 1 to arena size
        scaled = scaled * self._arena_size_in_meters / 2
        return scaled

    def _scale_coordinates_array(self, coordinates_array):
        """
        Scale array of pixel coordinates to real world meter coordinates
        """
        if coordinates_array is not None and coordinates_array.any():
            return self._vectorized_scale_coordinate(coordinates_array)
        return coordinates_array

    def _get_image(self):
        """
        Raises:
            ValueE
        """
        temp_latest_image = None
        with self._lock:
            temp_latest_image = self._latest_image

        if temp_latest_image is None:
            raise ValueError("Could not get image from video source")

        # temp_latest_image = self._crop_image(temp_latest_image)
        temp_latest_image = self._resize_image(temp_latest_image)
        return temp_latest_image

    def _get_ball_coordinates(self, image, debug=False):
        if image is None:
            image = self._get_image()
        hsv_image = self._blur_and_hsv(image)

        pink_balls_image, pink_mask = self._find_balls_by_color(
            hsv_image,
            image,
            self._low_pink_ball_color,
            self._high_pink_ball_color)
        yellow_balls_image, yellow_mask = self._find_balls_by_color(
            hsv_image,
            image,
            self._low_yellow_ball_color,
            self._high_yellow_ball_color)

        pink_coordinates = self._find_center_points(pink_mask)
        if debug:
            for coordinate in pink_coordinates:
                cv2.circle(image, (
                    coordinate[0],
                    coordinate[1]), 5, (0, 0, 255), -1)

        yellow_coordinates = self._find_center_points(yellow_mask)
        if debug:
            for coordinate in yellow_coordinates:
                cv2.circle(image, (
                    coordinate[0],
                    coordinate[1]), 5, (0, 255, 255), -1)

        if debug:
            cv2.imshow('Ball Coordinates', image)

        return (
            self._scale_coordinates_array(pink_coordinates),
            self._scale_coordinates_array(yellow_coordinates))

    def _get_robot_coordinates(
            self,
            image,
            striker_aruco_code,
            goalie_aruco_code,
            debug=False):
        '''
        Detect aruco markers from given image and return striker,
        goalie and enemy robot coordinates

        Parameters:
            image (Numpy array):Image from which to detect the Aruco markers
            striker_aruco_code (int): Striker's Aruco code
            goalie_aruco_code (int): Goalie's Aruco code
            debug (bool): If true the OpenCV images are shown to user

        Returns:
            Tuple:
                scaled_striker_pos (numpy array[float]):
                    striker's x and y coordinates
                striker_rot (float): striker's z-rotation
                scaled_goalie_pos (numpy array[float]):
                    goalie's x and y coordinates
                goalie_rot (float): goalie's z-rotation
                scaled_enemy_positions ([numpy array[float]]):
                    enemy x and y coordinates
        '''
        if image is None:
            image = self._get_image()

        (striker_pos_and_rot,
         goalie_pos_and_rot,
         enemy_pos_and_rot) = get_aruco_marker_pos_and_rot(
            image,
            striker_aruco_code,
            goalie_aruco_code,
            self._aruco_dict,
            self._parameters,
            self._camera_calib_params,
            self._ip_camera_params["size_of_marker"],
            debug)

        # print("#######striker_pos_and_rot")
        # print(striker_pos_and_rot)
        # print("#######goalie_pos_and_rot")
        # print(goalie_pos_and_rot)
        # print("#######enemy_pos_and_rot")
        # print(enemy_pos_and_rot)

        scaled_striker_pos = np.array([], dtype=np.float32)
        striker_rot = None
        if striker_pos_and_rot:
            scaled_striker_pos = \
                self._scale_coordinates_array(striker_pos_and_rot[0])
            striker_rot = striker_pos_and_rot[1][2]

        scaled_goalie_pos = np.array([], dtype=np.float32)
        goalie_rot = None
        if goalie_pos_and_rot:
            scaled_goalie_pos = \
                self._scale_coordinates_array(goalie_pos_and_rot[0])
            goalie_rot = goalie_pos_and_rot[1][2]

        scaled_enemy_positions = np.array([], dtype=np.float32)
        if enemy_pos_and_rot:
            scaled_enemy_positions = []
            for enemy in enemy_pos_and_rot:
                scaled_enemy_pos = \
                    self._scale_coordinates_array(enemy[0])
                scaled_enemy_positions.append(scaled_enemy_pos)
            scaled_enemy_positions = np.array(
                scaled_enemy_positions,
                dtype=np.float32)

        return (
            scaled_striker_pos,
            striker_rot,
            scaled_goalie_pos,
            goalie_rot,
            scaled_enemy_positions)

    def get_dynamic_item_coordinates(
            self,
            striker_aruco_code,
            goalie_aruco_code,
            debug=False):
        """
        Get coordinates to all dynamic items: robots, good and bad balls
        """
        image = self._get_image()
        if image is None:
            raise Exception("Could not get an image")

        (striker_pos,
         striker_rot,
         goalie_pos,
         goalie_rot,
         enemy_positions) = self._get_robot_coordinates(
            image,
            striker_aruco_code,
            goalie_aruco_code,
            debug)

        good_ball_positions, bad_ball_positions = self._get_ball_coordinates(
            image, debug)

        if debug:
            # cv2.waitKey is needed to show the image
            # when calling 'cv2.imshow()'
            cv2.waitKey(1)

        return (
            striker_pos,
            striker_rot,
            goalie_pos,
            goalie_rot,
            enemy_positions,
            good_ball_positions,
            bad_ball_positions)


# For testing
# Run this with "python -m ip_cam_connection.ip_cam_connector"
# From the project's root folder
def main(_):
    '''
    Connect to IP cam and print results
    '''
    params_file = FLAGS.params_file
    params = parse_options(params_file)

    striker_aruco_code = \
        params["game_params"]["agent_params"]["striker"]["aruco_code"]
    goalie_aruco_code = \
        params["game_params"]["agent_params"]["goalie"]["aruco_code"]

    ip_cam_connection = IPCamConnection(params["ip_camera_params"])
    ip_cam_connection.start()

    try:
        while True:
            start_time = time.time()
            (striker_pos,
             striker_rot,
             goalie_pos,
             goalie_rot,
             enemy_positions,
             good_ball_positions,
             bad_ball_positions) = \
                ip_cam_connection.get_dynamic_item_coordinates(
                     striker_aruco_code,
                     goalie_aruco_code,
                     debug=True)

            end_time = time.time()
            print("=== striker_coordinates")
            if striker_pos.any() and striker_rot:
                print(["{0:0.2f}".format(i) for i in striker_pos] +
                      ["{0:0.2f}".format(striker_rot)])
            print("=== goalie_coordinates")
            if goalie_pos.any() and goalie_rot:
                print(["{0:0.2f}".format(i) for i in goalie_pos] +
                      ["{0:0.2f}".format(goalie_rot)])
            print("=== enemy_positions")
            for enemy in enemy_positions:
                print(["{0:0.2f}".format(i) for i in enemy])
            print("=== good_ball_positions")
            for good_ball in good_ball_positions:
                print(["{0:0.2f}".format(i) for i in good_ball])
            print("=== bad_ball_positions")
            for bad_ball in bad_ball_positions:
                print(["{0:0.2f}".format(i) for i in bad_ball])
            print("FPS: {:02}, TotalTime: {:01.3f}".format(
                int(1 / (end_time - start_time)),
                end_time - start_time))
            print("")

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

    except KeyboardInterrupt:
        print("Closing")

    finally:
        ip_cam_connection.close()


if __name__ == "__main__":
    from absl import app
    from absl import flags
    from utils import parse_options

    flags.DEFINE_string(
        "params_file",
        "params.yaml",
        "Specify the path to params.yaml file",
        short_name="p")

    FLAGS = flags.FLAGS
    app.run(main)
