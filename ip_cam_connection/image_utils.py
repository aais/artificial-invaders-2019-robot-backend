import numpy as np
import math
import cv2
from cv2 import aruco


def _is_rotation_matrix(matrix):
    """
    Checks if a matrix is a valid rotation matrix.
    https://www.learnopencv.com/rotation-matrix-to-euler-angles/

    Args:
        matrix (?): OpenCV matrix

    Returns:
        boolean: Is the input matrix rotation matrix
    """
    r_t = np.transpose(matrix)
    should_be_identity = np.dot(r_t, matrix)
    identity = np.identity(3, dtype=matrix.dtype)
    n = np.linalg.norm(identity - should_be_identity)
    return n < 1e-6


def _rotation_matrix_to_euler_angles(rotation_matrix):
    """
    Calculates rotation matrix to euler angles
    The result is the same as MATLAB except the order
    of the euler angles ( x and z are swapped ).
    https://www.learnopencv.com/rotation-matrix-to-euler-angles/

    Args:
        rotation_matrix (int): Rotation matrix to transform

    Returns:
        numpy array [3]: x, y and z euler rotations
    """
    assert _is_rotation_matrix(rotation_matrix)
    sy = math.sqrt(
        rotation_matrix[0, 0] * rotation_matrix[0, 0] +
        rotation_matrix[1, 0] * rotation_matrix[1, 0])
    singular = sy < 1e-6
    if not singular:
        x = math.atan2(rotation_matrix[2, 1], rotation_matrix[2, 2])
        y = math.atan2(-rotation_matrix[2, 0], sy)
        z = math.atan2(rotation_matrix[1, 0], rotation_matrix[0, 0])
    else:
        x = math.atan2(-rotation_matrix[1, 2], rotation_matrix[1, 1])
        y = math.atan2(-rotation_matrix[2, 0], sy)
        z = 0

    return np.array([
            math.degrees(x),
            math.degrees(y),
            math.degrees(z)],
        dtype=np.float32)


def _drawDetectedMarkers(
        image, corners, detected_ids, tvecs,
        rvecs, mtx, dist, length_of_axis, rejected_img_points):
    """
    Draw axis to the detected aruco markers
    Draw Squares around detected aruco markers
    Draw the rejected aruco markers candidates
    """
    debug_image = image.copy()
    imaxis = aruco.drawDetectedMarkers(debug_image, corners, detected_ids)
    for i in range(len(tvecs)):
        imaxis = aruco.drawAxis(
            imaxis,
            mtx,
            dist,
            rvecs[i],
            tvecs[i],
            length_of_axis)

    aruco.drawDetectedMarkers(debug_image, corners, detected_ids)
    aruco.drawDetectedMarkers(
        debug_image,
        rejected_img_points,
        borderColor=(100, 0, 240))
    cv2.imshow('Detect Acuro Marker', debug_image)


def get_aruco_marker_pos_and_rot(
        image,
        striker_aruco_code,
        goalie_aruco_code,
        aruco_dict,
        parameters,
        camera_calib_params,
        size_of_marker,
        debug=False,
        length_of_axis=0.05):
    """
    Args:
        image (numpy array): Detected aruco marker ids
        striker_aruco_code (int): Striker's aruco marker's is
        goalie_aruco_code (int): Goalie's aruco marker's is
        aruco_dict (?):
        parameters (?):
        camera_calib_params (?):
        size_of_marker (?):
        debug (?):
        length_of_axis (?):

    Returns:
        tuple
            striker_pos_and_rot : [[x, y] [rot_x, rot_y, rot_z]]
            goalie_pos_and_rot : [[x, y] [rot_x, rot_y, rot_z]]
            enemy_pos_and_rot : [ [[x, y] [rot_x, rot_y, rot_z]] ]
    """
    mtx = np.array(camera_calib_params["mtx"], dtype=np.float32)
    dist = np.array(camera_calib_params["dist"], dtype=np.float32)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    corners, detected_ids, rejected_img_points = aruco.detectMarkers(
        gray,
        aruco_dict,
        parameters=parameters)

    # rvecs, tvecs = None
    # if detected_ids:
    rvecs, tvecs, _ = aruco.estimatePoseSingleMarkers(
        corners,
        size_of_marker,
        mtx,
        dist)

    (striker_pos_and_rot,
     goalie_pos_and_rot,
     enemy_pos_and_rot) = _get_pos_and_rot(
        detected_ids,
        striker_aruco_code,
        goalie_aruco_code,
        corners,
        rvecs)

    if debug and tvecs is not None and rvecs is not None:
        _drawDetectedMarkers(gray, corners, detected_ids, tvecs,
                             rvecs, mtx, dist, length_of_axis,
                             rejected_img_points)

    return (
        striker_pos_and_rot,
        goalie_pos_and_rot,
        enemy_pos_and_rot)


def _get_pos_and_rot(
        detected_ids,
        striker_aruco_code,
        goalie_aruco_code,
        corners,
        rvecs):
    """
    Calculates rotation matrix to euler angles
    The result is the same as MATLAB except the order
    of the euler angles ( x and z are swapped ).
    https://www.learnopencv.com/rotation-matrix-to-euler-angles/

    Args:
        detected_ids ([int]): Detected aruco marker ids
        striker_aruco_code (int): Striker's aruco marker's is
        goalie_aruco_code (int):Goalie's aruco marker's is
        corners (?): List of Detected aruco marker corner's
                     from aruco.detectMarkers
        rvecs (?): ?

    Returns:
        tuple
            striker_pos_and_rot : [[x, y] [rot_x, rot_y, rot_z]]
            goalie_pos_and_rot : [[x, y] [rot_x, rot_y, rot_z]]
            enemy_pos_and_rot : [ [[x, y] [rot_x, rot_y, rot_z]] ]
    """
    if (detected_ids is None or
            corners is None or
            rvecs is None):
        return ([], [], [])

    striker_pos_and_rot = []
    goalie_pos_and_rot = []
    enemy_pos_and_rot = []

    for id, corner_list, rvec in zip(detected_ids, corners, rvecs):
        center = np.mean(corner_list[0], axis=0, dtype=np.float32)
        dst, jacobian = cv2.Rodrigues(rvec[0])
        euler_angles = _rotation_matrix_to_euler_angles(dst)

        if id == striker_aruco_code:
            striker_pos_and_rot.append(center)
            striker_pos_and_rot.append(euler_angles)
        elif id == goalie_aruco_code:
            goalie_pos_and_rot.append(center)
            goalie_pos_and_rot.append(euler_angles)
        else:
            enemy_pos_and_rot.append([center, euler_angles])

    return (
        striker_pos_and_rot,
        goalie_pos_and_rot,
        enemy_pos_and_rot)
