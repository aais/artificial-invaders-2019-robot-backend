## Setup a virtual environment
This repo is tested with Python 3.6.8.

Create Python virtual environment if you want.
### Ubuntu
`virtualenv venv`
`source venv/bin/activate`
### Windows 10
`python -m venv venv`
`venv\Scripts\Activate`

## Install Python packages
`pip install -r requirements.txt`
**Note for Windows:** Download Shapely-package which has it's geos-dependency built in from link below. Choose the one which matches your system from [here](https://www.lfd.uci.edu/~gohlke/pythonlibs/#shapely). Install with `pip install Shapely-1.6.4.post2-cp37-cp37m-win_amd64.whl` or what ever is your system's package name. After installing Shapely wheel comment out `shapely` from requirements.txt and run `pip install -r requirements.txt`.


## Camera calibration
Original code is from [here](https://mecaruco.readthedocs.io/en/latest/notebooks_rst/aruco_calibration.html)

1) Run command `python camera_calibration/camera_calibration.py -m=gen_chessboard`
2) Take images of the generated `charuco_chessboard.pdf` with the camera to be calibrated and put the in `camera_calibration/calibration_images/`
3) Run command `python camera_calibration/camera_calibration.py -m=calibration` and a `calib-params.json` is created

## Create Aruco marker
Run command `python -m camera_calibration.aruco_marker_generator -c=2 -s=700 -b=100` and an aruco marker is generated to the root folder

## Test Backend to Frontend connection
- Start two sessions of robot-frontend locally or on one or two Raspberry Pis. Set the striker and goalie ip_addesses and ports in `params.yaml` in `agent_params`
- Run `python rpi_connection_test.py` and you should see the following:
  ```
  Goalie says: True
  Striker says:True

  Goalie says: True
  Striker says:True
  ```

## Generate proto codes
### gRPC code for Robot communication
- Run the following command in project root to generate the gRPC code from the .proto-file: 
    ```sh
    python -m grpc_tools.protoc --proto_path=. --python_out=. --grpc_python_out=. ./robot_communication/RobotCommunication.proto
    ```
### gRPC code for Robot Reporter
- Run the following command in project root to generate the gRPC code from the .proto-file: 
    ```sh
    python -m grpc_tools.protoc --proto_path=. --python_out=. --grpc_python_out=. ./robot_reporter/RobotReporter.proto
    ```
### gRPC code for Unity communication
- Run the following command in project root to generate the gRPC code from the .proto-file: 
    ```sh
    python -m grpc_tools.protoc --proto_path=. --python_out=. --grpc_python_out=. ./unity_communication/UnityCommunication.proto
    ```

## Test IP camera and observation generation
On Android you can download [this app](https://play.google.com/store/apps/details?id=com.pas.webcam&hl=en) to get an IP-camera

Run command `python -m robot_game.robot_game -m=dynamic` or `python -m robot_game.robot_game -m=staic` to see the observations of the robot

The `robot_game.py` detects yellow and pink floor balls, enemy and own robots  in `dynamic`-mode. If mode is `static` the `.json` files from `arena_params`-folder are used to detect walls, blueGoal and redGoal. You need an Aruco code in the camera view which acts as the robot and as the center of observations.

## Test Robot Reporter servicer
- Run `python -m robot_reporter.robot_reporter_servicer` in project root. Start Raspberry Pis and when they wake up they autostart the robot frontend which reports the IP-address, robot communication port and agent type to Robot Reporter servicer

## Run Robot Backend
- Set the `params.yaml` parameters.
- Run Unity with the `BrainRemoteUsage`-scene
- Run `python robot_backend.py` in project root to start
- Start Robot Raspberry Pis

Robots report their information to the robot backend after which the robot backend starts generating observations from the camera image for both robots. The observations are sent to Unity which responses with predicted actions for both robots. Robot backend converts the actions into integer speed values for robots' motors which are sent to the Robots.